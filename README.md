# Keeping up architecture with Neo4J graph database

e archisurance gist is te maken door in een bash shell uitvoeren van het commando:

```
 ./run.sh adoc/archisurance.adoc html/archisurance.html
```

Vanuit de Neo4J browser op dezelfde server is de gist te gebruiken met het commando

```
:play http://localhost:8001/html/archisurance.html
```
 
Te installeren software:

* Ruby
* Python

Ruby wordt gebruik voor het vanuit asciidoc genereren van html.
asciidocter wordt automatisch geinstalleerd door het run script. Overige onderdelen moeten handmatig geinstalleerd worden.

```
gem install concurrent-ruby
```

Om de gist te gebruiken moet deze via een webserver aangeboden worden. Hiervoor wordt python gebruikt

Python wordt gebruikt als webserver voor het lokaal testen van de gist.
start een webserver met

```
python http-server.py
```

Als laatste moet er, bij een andere server dan localhost,  nog voor gezorgd worden dat de server vanuit de neo4j Browser gebruikt kan worden.

Neo4j heeft een beveiliging waardoor alleen gists vanaf guides.neo4j.com en localhost gebruikt kunnen worden.
Bij een andere locatie moet de base-url toegevoegd worden aan een whitelist

In de file neo4j.conf moet dan het volgende toegevoegd worden

```
# comma separated list of base-urls, or * for everything
browser.remote_content_hostname_whitelist=*
```

Aanpassen van de style kan met een grassfile

```
:style http://localhost:8001/html/graphstyle.grass
```

